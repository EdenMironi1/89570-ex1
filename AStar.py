'''
This file should be runnable to get AStar using 
$ python AStar.py
'''

from queue import PriorityQueue
from Utils.Functions import distance_func, huristic_function
from Utils.Node import Node
from csv_map import csv_map

def AStar(source, target, cost_function=distance_func):
    # graph = csv_map.instance()
    # queue = PriorityQueue()
    # queue.put(source, 0)

    # open = dict()
    # total_cost = dict()
    
    # open[source] = None
    # total_cost[source] = 0

    # while not queue.empty():
    #     current = queue.get()
    #     if current == target:
    #         break
    #     for link in graph[current].links:
    #         new_cost = total_cost[current] + cost_function(link.source, link.target)
    #         if link.target not in total_cost or new_cost < total_cost[link.target]:
    #             total_cost[link.target] = new_cost
    #             priority = new_cost + huristic_function(graph[link.target].lat,graph[link.target].lon , graph[target].lat, graph[target].lon)
    #             queue.put(link.target, priority)
    #             open[link.target] = current

    # return total_cost, route list
    graph = csv_map.instance()
    queue = PriorityQueue()
    queue.put((0, Node([graph[source]], source)))
    closed_list = set()
    while not queue.empty():
        pair = queue.get()
        current = graph[pair[1].curr]
        if current.index == target:
            return pair[0], pair[1].route
        closed_list.add(current.index)
        for link in current.links:
            new_path = list(pair[1].route)
            new_path.append(graph[link.target])
            new_cost = pair[0] + cost_function(link.source, link.target) + huristic_function(graph[link.target].lat,graph[link.target].lon , graph[target].lat, graph[target].lon)
            if link.target not in closed_list and Node([], link.target) not in queue.queue:
                queue.put((new_cost, Node(new_path, link.target)))
            elif Node([], link.target) in queue.queue:
                x = next(x for x in queue.queue if link.target == x[1].curr)
                if (new_cost < x[0]):
                    queue.queue.remove(x)
                    queue.put((new_cost, Node(new_path, link.target)))

        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    route, cost = AStar(79,84, distance_func)
    print(" ".join(map(lambda x: str(x),route)))
    # print_route(route[1])

