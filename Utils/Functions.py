from csv_map import csv_map
from haversine import haversine
from ways.info import SPEED_RANGES


def distance_func(source_ind, target_ind):
    graph = csv_map.instance()
    link_source_target = next((link for link in graph[source_ind].links if link.target == target_ind), None)
    if link_source_target is not None:
        return link_source_target.distance / SPEED_RANGES[link_source_target.highway_type][1]
    else: 
        99999


def huristic_function(lat1, lon1, lat2, lon2):
    return haversine(lat1, lon1, lat2, lon2) / 110.0
