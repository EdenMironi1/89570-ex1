from functools import total_ordering

@total_ordering
class Node:
    def __init__(self, route, curr):
      self.route = route
      self.curr = curr
    
    def __eq__(self, other):
      if isinstance(other, Node):
        return self.curr == other.curr
      return self.curr == other[1].curr

    def __repr__(self):
      return f"<{self.curr}>"

    def __lt__(self, node):
      return self.curr < node.curr
    
    def __ne__(self, other):
      return not (self == other)

    def __hash__(self):
        return hash(self.curr)