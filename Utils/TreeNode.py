from functools import total_ordering

from csv_map import csv_map
from ways.info import SPEED_RANGES

def ordered_set(coll):
  return dict.fromkeys(coll).keys()

def distance_func(link):
    graph = csv_map.instance()
    if link is not None:
        return link.distance / SPEED_RANGES[link.highway_type][1]
    else: 
        99999
        
@total_ordering
class TreeNode:
    def __init__(self, state, parent=None, action=None, path_cost=0):
      self.state = state
      self.parent = parent
      self.action = action
      self.path_cost = path_cost
      self.depth = 0
      if parent:
        self.depth = parent.depth + 1

    def expand(self):
        graph = csv_map.instance()
        return ordered_set([self.child_node(link)
            for link in graph[self.state].links()])

    def child_node(self, link):
      next_state = link.target
      next_node = TreeNode(next_state, self, link,
                  self.path_cost+distance_func(link))
      return next_node
    
    def solution(self):
      return [node.action for node in self.path()[1:]]

    def path(self):
      node, path_back = self, []
      while node:
          path_back.append(node)
          node = node.parent
      return list(reversed(path_back))
    def __repr__(self):
      return f"<{self.state}>"

    def __lt__(self, node):
      return self.state < node.state
    
    def __eq__(self, other):
      return isinstance(other, TreeNode) and self.state == other.state

    def __ne__(self, other):
      return not (self == other)

    def __hash__(self):
        return hash(self.state)