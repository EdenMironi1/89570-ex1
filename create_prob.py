'''
This file should be runnable to print create problems using 
$ python create_prob.py
'''

from asyncio.windows_events import NULL
from collections import namedtuple, Counter
from ways import load_map_from_csv
from ways import info
import random 


# define class Problem
Problem = namedtuple('Problem',
           ['start',       #  String
            'end',  #  String
           ])

def print_probs():
    problems = []
    junctions = (load_map_from_csv()).junctions()
    roads = list(filter(lambda road: len(road.links) > 0, junctions))
    for i in range(99):
        rand = random.randint(1, 5)
        curr_junc = roads[i]
        if len(curr_junc.links) > 1 :
            list_links = list(curr_junc.links)
            list_links.sort(key=lambda x: x.target)
            next_junc = roads[list_links[1].target]
        else: 
            next_junc = roads[curr_junc.links[0].target]
        for j in range(rand):
            next_next_junc = roads[next_junc.links[random.randint(0, len(next_junc.links) - 1)].target]
            if next_next_junc.index == curr_junc.index:
                break
            curr_junc = next_junc
            next_junc = next_next_junc
        problems.append(Problem(start="s"+str(i), end="t"+str(next_junc.index)))
    import csv
    with open('problems.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        for x in problems:
            writer.writerow([x.start, x.end])
        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    print_probs()

 