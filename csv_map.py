from ways import load_map_from_csv

class csv_map():
    _map = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._map is None:
            cls._map = load_map_from_csv()
        return cls._map
