import sys
from Utils.TreeNode import TreeNode


def depth_limited_search(source, target, limit=5):
    frontier = [(TreeNode(source))]  # Stack
    while frontier:
        node = frontier.pop()
        if node.state == target:
          return node.solution()
        if node.depth<limit:
          frontier.extend(node.expand())
    return None 

def iterative_deepening_search(source, target):
  for depth in range(1, sys.maxsize):
    result = depth_limited_search(source, target, depth)
    if result:
      return result
  return None, iterative_deepening_search
    

def print_route(route):
    for junction in route:
        print(junction.index , end =" ")
        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    route = iterative_deepening_search(5,7)

