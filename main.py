'''
Parse input and run appropriate code.
Don't use this file for the actual work; only minimal code should be here.
We just parse input and call methods from other modules.
'''

#do NOT import ways. This should be done from other files
#simply import your modules and call the appropriate functions


from AStar import AStar
from Utils.Functions import distance_func
from haversine import haversine
from ida_star import iterative_deepening_search
from ucs import ucs


def huristic_function(lat1, lon1, lat2, lon2):
    return haversine(lat1, lon1, lat2, lon2) / 110.0


def find_ucs_rout(source, target, cost_function=distance_func):
    'call function to find path, and return list of indices'
    return ucs(source, target, distance_func)[1]


def find_astar_route(source, target):
    'call function to find path, and return list of indices'
    route, _ = AStar(source, target, distance_func)
    return route


def find_idastar_route(source, target):
    return iterative_deepening_search(source, target)

def dispatch(argv):
    from sys import argv
    source, target = int(argv[2]), int(argv[3])
    if argv[1] == 'ucs':
        path = find_ucs_rout(source, target)
    elif argv[1] == 'astar':
        path = find_astar_route(source, target)
    elif argv[1] == 'idastar':
        path = find_idastar_route(source, target)
    print(' '.join(str(j) for j in path))


if __name__ == '__main__':
    from sys import argv
    dispatch(argv)
