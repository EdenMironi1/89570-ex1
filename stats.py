'''
This file should be runnable to print map_statistics using 
$ python stats.py
'''

from collections import namedtuple, Counter
from ways import load_map_from_csv
from ways import info


def map_statistics(roads):
    '''return a dictionary containing the desired information
    You can edit this function as you wish'''
    Stat = namedtuple('Stat', ['max', 'min', 'avg'])
    count_road = 0
    min_road = 9999
    max_road = 0
    sum_dist = 0
    min_dist = 9999
    max_dist = 0  
    road_type = [0] * len(info.ROAD_TYPES)   
    for road in roads.junctions():
        count_road += len(road.links)
        min_road = len(road.links) if len(road.links) < min_road else min_road
        max_road = len(road.links) if len(road.links) > max_road else max_road
        for link in road.links:
            sum_dist += link.distance
            min_dist = link.distance if link.distance < min_dist else min_dist
            max_dist = link.distance if link.distance > max_dist else max_dist
            road_type[link.highway_type] += 1
    return {
        'Number of junctions' : len(roads),
        'Number of links' : count_road,
        'Outgoing branching factor' : Stat(max=max_road, min=min_road, avg=(count_road/len(roads))),
        'Link distance' : Stat(max=max_dist, min=min_dist, avg=(sum_dist/count_road)),
        # value should be a dictionary
        # mapping each road_info.TYPE to the no' of links of this type
        'Link type histogram' : { info.ROAD_TYPES[i] : road_type[i] for i in range(0, len(road_type) ) },  # tip: use collections.Counter
    }


def print_stats():
    for k, v in map_statistics(load_map_from_csv()).items():
        print('{}: {}'.format(k, v))

        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    print_stats()

