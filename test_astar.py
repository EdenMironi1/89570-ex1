'''
This file should be runnable to get ucs using 
$ python ucs.py
'''

from collections import namedtuple
from AStar import AStar
from Utils.Functions import huristic_function
from csv_map import csv_map
from ways import tools
import matplotlib as plt


# define class Problem
Problem = namedtuple('Problem',
       ['source', 'target',  #  int 
       ])
       

def _make_problem(source, target):
    'This function is for local use only'
    return Problem(int(source[1:]), int(target[1:]))

def read_problems(filename='problems.csv', start=0, count=100):
    import csv

    from itertools import islice

    with tools.dbopen(filename, 'rt') as f:
        it = islice(f, start, start+count)
        lst = {_make_problem(*row) for row in csv.reader(it)}
    return lst

        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    graph = csv_map.instance()
    outputLines = []
    plot_data_huristic = []
    plot_data_cost = []
    for prob in read_problems():
        print(prob.source, prob.target)
        curr_astar, curr_cost = AStar(prob.source, prob.target)
        line = " ".join(map(lambda x: str(x),curr_astar)) + " - זמן הנסיעה - " + str(curr_cost[prob.target])
        outputLines.append(line)
        plot_data_huristic.append(huristic_function(graph[prob.source].lat,graph[prob.source].lon , graph[prob.target].lat, graph[prob.target].lon))
        plot_data_cost.append(curr_cost[prob.target])
        print(line)
    with open('resultst/AStarRuns.txt', 'w') as f:
        f.writelines(outputLines)
    plt.scatter(plot_data_huristic, plot_data_cost)
    plt.xlabel('huristic')
    plt.ylabel('actual')
    plt.axis('scaled')
    plt.show()
