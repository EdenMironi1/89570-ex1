'''
This file should be runnable to get ucs using 
$ python ucs.py
'''

from collections import namedtuple
from ucs import print_route, ucs
from ways import tools


# define class Problem
Problem = namedtuple('Problem',
       ['source', 'target',  #  int 
       ])
       

def _make_problem(source, target):
    'This function is for local use only'
    return Problem(int(source[1:]), int(target[1:]))

def read_problems(filename='problems.csv', start=0, count=100):
    import csv

    from itertools import islice

    with tools.dbopen(filename, 'rt') as f:
        it = islice(f, start, start+count)
        lst = {_make_problem(*row) for row in csv.reader(it)}
    return lst

        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    outputLines = []
    for prob in read_problems():
        print(prob.source, prob.target)
        curr_ucs = ucs(prob.source, prob.target)
        line = " ".join(map(lambda x: str(x.index), curr_ucs[1].route)) + " - " + str(curr_ucs[0]) 
        outputLines.append(line)
        print(" ".join(map(lambda x: str(x.index), curr_ucs[1].route)) + " - " + str(curr_ucs[0]))
    with open('results/UCSRuns.txt', 'w') as f:
        f.writelines(outputLines)
