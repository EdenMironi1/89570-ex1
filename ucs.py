'''
This file should be runnable to get ucs using 
$ python ucs.py
'''
from queue import PriorityQueue
from Utils.Functions import distance_func
from Utils.Node import Node
from csv_map import csv_map

def ucs(source, target, cost_function=distance_func):
    graph = csv_map.instance()
    queue = PriorityQueue()
    queue.put((0, Node([graph[source]], source)))
    closed_list = set()
    while not queue.empty():
        pair = queue.get()
        current = graph[pair[1].curr]
        if current.index == target:
            return pair
        closed_list.add(current.index)
        for link in current.links:
            new_path = list(pair[1].route)
            new_path.append(graph[link.target])
            if link.target not in closed_list and Node([], link.target) not in queue.queue:
                queue.put(((pair[0] + cost_function(link.source, link.target)), Node(new_path, link.target)))
            elif Node([], link.target) in queue.queue:
                x = next(x for x in queue.queue if link.target == x[1].curr)
                if (pair[0] + cost_function(link.source, link.target) < x[0]):
                    queue.queue.remove(x)
                    queue.put(((pair[0] + cost_function(link.source, link.target)), Node(new_path, link.target)))


def print_route(route):
    for junction in route:
        print(junction.index , end =" ")
        
if __name__ == '__main__':
    from sys import argv
    assert len(argv) == 1
    x = Node([1], 2)
    route = ucs(46 ,536417, distance_func)
    print(" ".join(map(lambda x: str(x.index), route[1].route)))

